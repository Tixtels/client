import { getRawXY, getXY, detectColor, convertColor } from '../../common/utils';
export { getRawXY, getXY, detectColor, convertColor };

import { updateTixtel } from './net.js';
import { chunkSize, options, chunks, offsets } from './data.js';

// todo remove these
EventTarget.prototype.addEvent = function(es, fn) {
  es.split(' ').forEach(e => this.addEventListener(e, fn));
};

export function rgb(r, g, b) {
  if(Array.isArray(r)) return 'rgb(' + r + ')';
  else return 'rgb(' + r + ',' + g + ',' + b + ')';
}
export function isFocused() { return ['INPUT', 'TEXTAREA'].includes(document.activeElement.tagName); }
export function randomInt(a, b) { return Math.floor(Math.random() * (Math.trunc(b) - Math.trunc(a + 1))) + Math.trunc(a); }
export function randomColor() { return convertColor(randomInt(0, 16777215), 'RGBArr'); }
export function eq(a, b) { return (Array.isArray(a) && Array.isArray(b) ? a.join() === b.join() : a === b); }

// DOM/html utils

export function setTheme(css) { byId('theme').innerHTML = css; }
export function byId(id){ return document.getElementById(id); }
export function html(tag, data) {
  const e = document.createElement(tag);
  for(const k in data) e[k] = data[k];
  return e;
}
export function htmlEscape(string) {
  return string
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;');
}

export function findBlock(mouseX, mouseY) {
  mouseX -= offsets.x;
  mouseY -= offsets.y;
  const chunkX = Math.floor(mouseX / options.blockSize  / chunkSize);
  const chunkY = Math.floor(mouseY / options.blockSize  / chunkSize);
  let blockX = Math.floor(mouseX / options.blockSize) % chunkSize;
  if(blockX < 0) blockX += chunkSize;
  let blockY = Math.floor(mouseY / options.blockSize) % chunkSize;
  if(blockY < 0) blockY += chunkSize;
  return [chunkX, chunkY, blockX, blockY];
}

export function getTixtel(x, y) {
  const [chunkX, chunkY, blockX, blockY] = getRawXY(x, y);
  return chunks[[chunkX, chunkY]].blocks[blockY + chunkSize * blockX];
}

export function setColor(x, y, color, local) {
  const [chunkX, chunkY, blockX, blockY] = getRawXY(x, y);
  const t = chunks[[chunkX, chunkY]].blocks[blockY + chunkSize * blockX];
  t.setColor(color);
  if(!local) updateTixtel(x, y, color, t.char);
}

export function setChar(x, y, char, local) {
  const [chunkX, chunkY, blockX, blockY] = getRawXY(x, y);
  const t = chunks[[chunkX, chunkY]].blocks[blockY + chunkSize * blockX];
  t.setChar(char);
  if(!local) updateTixtel(x, y, t.color, char);
}

export function setTixtel(x, y, char, color, local) {
  if(char) setChar(x, y, char, local);
  if(color) setColor(x, y, color, local);
}

export function lineAlgo(x0, y0, x1, y1, fn) {
  let dx =  Math.abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
  let dy = -Math.abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
  let err = dx+dy, e2;
  while(true) {
    fn(x0, y0);
    if (x0 === x1 && y0 === y1) break;
    e2 = 2 * err;
    if (e2 >= dy) { err += dy; x0 += sx; }
    if (e2 <= dx) { err += dx; y0 += sy; }
  }
}

export function line(startX, startY, endX, endY, color) {
  lineAlgo(startX, startY, endX, endY, (x, y) => setColor(x, y, color));
}

export function rectangle(startX, startY, endX, endY, color) {
  const [dx, dy] = [endX - startX, endY - startY];
  line(startX, startY, startX, endY, color);
  line(startX, startY, endX, startY, color);
  line(startX + dx, startY, endX, startY + dy, color);
  line(startX, startY + dy, startX + dx, endY, color);
}

// http://members.chello.at/easyfilter/bresenham.html
export function plotEllipseRectAlgo(x0, y0, x1, y1, fn) {
  let a = Math.abs(x1-x0), b = Math.abs(y1-y0), b1 = b&1; /* values of diameter */
  let dx = 4*(1-a)*b*b, dy = 4*(b1+1)*a*a; /* error increment */
  let err = dx+dy+b1*a*a, e2; /* error of 1.step */

  if (x0 > x1) { x0 = x1; x1 += a; } /* if called with swapped points */
  if (y0 > y1) y0 = y1; /* .. exchange them */
  y0 += (b+1)/2; y1 = y0-b1;   /* starting pixel */
  a *= 8*a; b1 = 8*b*b;

  do {
    fn(x1, y0); /*   I. Quadrant */
    fn(x0, y0); /*  II. Quadrant */
    fn(x0, y1); /* III. Quadrant */
    fn(x1, y1); /*  IV. Quadrant */
    e2 = 2*err;
    if (e2 <= dy) { y0++; y1--; err += dy += a; }  /* y step */
    if (e2 >= dx || 2*err > dy) { x0++; x1--; err += dx += b1; } /* x step */
  } while (x0 <= x1);

  while (y0-y1 < b) {  /* too early stop of flat ellipses a=1 */
    fn(x0-1, y0); /* -> finish tip of ellipse */
    fn(x1+1, y0++);
    fn(x0-1, y1);
    fn(x1+1, y1--);
  }
}

export function plotEllipseRect(x0, y0, x1, y1, color) {
  plotEllipseRectAlgo(x0, y0, x1, y1, (x, y) => setColor(x, y, color));
}

export function intervalLoop(fn, n, cd, end) {
  let i = 0;
  const interval = setInterval(() => {
    fn(i); i++;
    if(i == n) {
      clearInterval(interval);
      if(end) end();
      info.ended = true;
    }
  }, cd);
  const info =  {
    end: () => clearInterval(interval),
    get index() { return i; }
  };
  return info;
}