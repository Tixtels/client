import { findBlock } from './utils.js';
import { requestChunk, pendingChunks } from './net.js';
import { renderGrid, renderCursors } from './camera.js';
import { chunkSize, chunks, game, options, ctx, offsets } from './data.js';
import Chunk from './chunk.js';

export function add(chunk) {
  if(!chunk || !(chunk instanceof Chunk)) throw new Error('Invalid chunk provided');
  chunks[[chunk.x, chunk.y]] = chunk;
}

export function rerender() {
  for(let ci in chunks) {
    const c = chunks[ci];
    if(c.isVisible()) {
      ctx.drawImage(c.canvas, offsets.x + c.x * chunkSize * options.blockSize, offsets.y + c.y * chunkSize * options.blockSize);
    }
  }

  const topleft = findBlock(0, 0);
  const lowerright = findBlock(game.width, game.height);
  lowerright[0] += 1;
  lowerright[1] += 1;
  for(let x = topleft[0]; x < lowerright[0]; x++) {
    for(let y = topleft[1]; y < lowerright[1]; y++) {
      if(!chunks[[x, y]] && !pendingChunks.has(x + ',' + y)) {
        requestChunk(x, y);
      }
    }
  }
  if(options.blockSize > 4) renderGrid();
  renderCursors();
}