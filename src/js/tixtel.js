import { options, ctx, offsets, chunkSize } from './data.js';
import * as Camera from './camera.js';
import { rgb, eq } from './utils.js';

export default class Tixtel {
  constructor(x, y, parent) {
    this.x = x;
    this.y = y;
    this.renderX = x * options.blockSize;
    this.renderY = y * options.blockSize;
    this.parent = parent;
    this.color = [255, 255, 255];
    this.char = '\u0000';
  }

  render() {
    this.parent.ctx.beginPath();
    this.parent.ctx.fillStyle = rgb(...this.color);
    this.parent.ctx.rect(this.renderX, this.renderY, options.blockSize, options.blockSize);
    this.parent.ctx.fill();
    if(this.char !== '\u0000') {
      this.parent.ctx.fillStyle = rgb(...this.color.map(n => 255 - n));
      this.parent.ctx.font = (options.blockSize + 8) + 'px Ubuntu Mono';
      this.parent.ctx.save();
      this.parent.ctx.clip();
      this.parent.ctx.fillText(this.char, this.renderX + options.blockSize / 2, this.renderY + options.blockSize / 2);
      this.parent.ctx.restore();
    }
  }

  setChar(char) {
    if(this.char !== char) {
      this.char = char;
      this.render();
      this.renderGridPiece();
    }
  }

  setColor(color) {
    if(!eq(color, this.color)) {
      this.color = color;
      this.render();
      this.renderGridPiece();
    }
  }

  renderGridPiece() {
    if(options.showGrid) {
      ctx.fillStyle = Camera.gridPattern;
      ctx.fillRect(
        offsets.x + (this.parent.x * chunkSize + this.x) * options.blockSize,
        offsets.y + (this.parent.y * chunkSize + this.y) * options.blockSize,
        options.blockSize, options.blockSize
      );
    }
  }
}