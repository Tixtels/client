// const { chunkSize } = require('../config.json'); || cannot use commonjs or webpack tree shaking will not work
const chunkSize = 16;

function getRawXY(x, y) {
  let blockX = x % chunkSize, blockY = y % chunkSize;
  if(blockX < 0) blockX += chunkSize;
  if(blockY < 0) blockY += chunkSize;
  return [Math.floor(x / chunkSize), Math.floor(y / chunkSize), blockX, blockY];
}

function getXY(chunkX, chunkY, blockX, blockY) {
  return [chunkX * chunkSize + blockX, chunkY * chunkSize + blockY];
}

function detectColor(color) {
  switch(typeof color) {
    case 'number': return 'RGBInt';
    case 'string': return 'Hex';
  }
  if(Array.isArray(color)) return 'RGBArr';
  throw new Error('Color could not be detected!');
}

function convertColor(color, to, from = detectColor(color)) {
  if(from == 'RGBInt' && to == 'RGBArr') return [(color >> 16) & 255, (color >> 8) & 255, color & 255];
  if(from == 'RGBInt' && to == 'Hex') return ('00000' + (color).toString(16)).slice(-6);

  if(from == 'RGBArr' && to == 'RGBInt') return 256 * 256 * color[0] + 256 * color[1] + color[2];
  if(from == 'RGBArr' && to == 'Hex') return ('00000' + (256 * 256 * color[0] + 256 * color[1] + color[2]).toString(16)).slice(-6);

  if(from == 'Hex' && color.startsWith('#')) color = color.slice(1);
  if(from == 'Hex' && to == 'RGBInt') return parseInt(color, 16);
  if(from == 'Hex' && to == 'RGBArr') return convertColor(convertColor(color, 'RGBInt'), 'RGBArr');
}

module.exports = { getRawXY, getXY, detectColor, convertColor };