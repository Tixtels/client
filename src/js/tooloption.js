import { html } from './utils.js';
import * as Windows from './windows.js';
import * as Tools from './tools.js';

export default class ToolOption {
  constructor(tool) {
    if(!(tool instanceof Tools.tool)) throw new Error('Invalid tool provided');
    this.lastElement = null;
    this.currentLabel = null;
    this.tool = tool;
    tool.c = html('div', { className: 'cog' });
    tool.d.appendChild(tool.c);
    this.win = Windows.add(class extends Windows.window {
      constructor() {
        super(tool.name + ' options', {
          hideOnClose: true
        });
        this.hide();
      }
    });
    tool.c.addEventListener('click', () => this.open());
  }

  open() {
    const x = this.tool.d.offsetLeft + this.tool.d.offsetParent.offsetLeft,
      y = this.tool.d.offsetTop + this.tool.d.offsetParent.offsetTop + this.tool.d.offsetHeight;
    this.win.move(x, y);
    this.win.show();
    return this;
  }

  get target() {
    return this.currentLabel || this.lastElement;
  }

  addSlider(min = 0, max = 100, oninput) {
    this.target.appendChild(this.lastElement = html('input', {
      type: 'range', min, max,
      className: 'beta', oninput
    }));
    return this;
  }

  addLabel(text = '') {
    if(this.currentLabel) throw new Error('Nesting labels is not possible, please exitLabel() first');
    this.win.container.appendChild(this.lastElement = html('label', {
      innerText: text
    }));
    return this;
  }

  enterLabel() {
    if(!(this.lastElement instanceof HTMLLabelElement)) throw new Error('Last element is not a label');
    this.currentLabel = this.lastElement;
    return this;
  }

  exitLabel() {
    this.currentLabel = null;
    return this;
  }

  appendText(text) {
    this.target.insertAdjacentText('beforeend', text);
    return this;
  }

  addCheckbox(checked, onchange) {
    this.target.appendChild(this.lastElement = html('input', {
      type: 'checkbox',
      checked, onchange
    }));
    return this;
  }

  addSelect(options, onchange) {
    const select = this.lastElement = html('select', { onchange });
    options.forEach(e => {
      select.options.add(new Option(e.text, e.value || e.text));
    });
    this.target.appendChild(select);
    this.lastElement = select;
    return this;
  }

  addInput(type, placeholder, oninput) {
    this.target.appendChild(this.lastElement = html('input', { type, placeholder, oninput }));
    return this;
  }

  addButton(text, onclick) {
    this.target.appendChild(this.lastElement = html('button', { innerText: text, onclick }));
    return this;
  }

  getLast() {
    return this.lastElement;
  }
}