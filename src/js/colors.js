import * as Windows from './windows.js';
import { convertColor, rgb } from './utils.js';
import { defPalette } from './data.js';

const win = Windows.add(class extends Windows.window {
  constructor() {
    super('Colors');
  }
});

const d = createDiv('#ffffff', null, null, 'input');
d.type = 'color';
d.onchange = () => add(d.value);
d.style.padding = '0';
win.container.appendChild(d);

export let color = [0, 0, 0];
export let colors = {};

load();

export function save() {
  localStorage.colors = JSON.stringify(Object.keys(colors));
}

export function load() {
  if(localStorage.colors) {
    const cols = JSON.parse(localStorage.colors).map(col => col.split(',').map(c => +c));
    color = cols[0];
    bulkAdd(cols);
  } else bulkAdd(defPalette);
}

export function choose(col) {
  colors[color].style.outline = '';
  colors[col].style.outline = '2px solid red';
  color = col;
}

function createDiv(col, onChoose, onRemove, elm = 'div') {
  const d = document.createElement(elm);
  d.style.backgroundColor = rgb(col);
  d.className = 'color';
  d.title = col;
  d.onclick = onChoose;
  d.oncontextmenu = e => {
    onRemove();
    e.preventDefault();
  };
  return d;
}

export function add(col, dontChoose, dontSave) {
  if(typeof col === 'string') col = convertColor(col, 'RGBArr');
  if(colors.hasOwnProperty(col)) return false;
  let d = createDiv(col, () => choose(col), () => remove(col));
  win.container.insertBefore(d, win.container.lastElementChild);
  colors[col] = d;
  if(!dontChoose) choose(col);
  if(!dontSave) save();
}

export function bulkAdd(colors) {
  colors.forEach(col => add(col));
}

export function remove(col, dontSave) {
  win.container.removeChild(colors[col]);
  delete colors[col];
  if(!dontSave) save();
}

export function removeAll() {
  for(const col in colors) win.container.removeChild(colors[col]);
  colors = {};
}