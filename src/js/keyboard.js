import { go, page, toggleGrid, zoomIn, zoomOut, move } from './camera.js';
import { input as chatInput } from './chat.js'; // The CCC Gang
import { rerender } from './chunks.js';
import { isFocused } from './utils.js';
import { select } from './tools.js';
import { options } from './data.js';

const keys = new Set();
export const toolKeybinds = {};

export function isPressed(key) {
  return keys.has(key);
}

window.addEvent('keydown', e => {
  if(!e.key) return; // happens when browser autofills e.g. password
  const key = e.key.toLowerCase();
  keys.add(key);
  if(isFocused()) {
    if(key === 'enter' && document.activeElement === chatInput && !chatInput.value)
      chatInput.blur();
    return;
  }
  if(options.rank === 2 && key.startsWith('page')) page(key.slice(4));
  switch(key) {
    case 'end':
      if(e.shiftKey) document.body.innerHTML = '<h1 style=\'text-align: center;\'>Fin.</h1>';
      break;
    case 'enter':
      chatInput.focus();
      e.preventDefault();
      break;
    case 'home': if(e.shiftKey) go(0, 0); break;
    case 'g':
      toggleGrid();
      break;
    case '=':
    case '+':
      zoomIn();
      break;
    case '-':
      zoomOut();
      break;
    default:
      if(toolKeybinds.hasOwnProperty(key))
        select(toolKeybinds[key]);
  }
});

window.addEvent('keyup', e => {
  if(!e.key) return;
  const key = e.key.toLowerCase();
  keys.delete(key);
});

// why is this here
(function cameraTick() {
  if(!isFocused()) {
    if(isPressed('arrowup')) move('up');
    if(isPressed('arrowleft')) move('left');
    if(isPressed('arrowdown')) move('down');
    if(isPressed('arrowright')) move('right');
  }

  rerender();
  requestAnimationFrame(cameraTick);
})();