import * as Windows from './windows.js';
import { send as wsSend, on } from './ws.js';
import { html as colorize } from '../../common/colors.js';
import { html, htmlEscape } from './utils.js';
import PacketReader from '../../PacketReader.js';
import PacketWriter from '../../PacketWriter.js';

export const win = Windows.add(class extends Windows.window {
  constructor() { super('Chat'); }
});

let lmd = 0;

export const msglog = html('div', { id: 'chat_messages' });
win.container.appendChild(msglog);

on('message', e => {
  const buf = PacketReader(e.data);
  if(buf.type !== 'c_chat_message') return;
  const { sender, message } = buf;
  append(htmlEscape(sender), colorize(htmlEscape(message)));
  msglog.scrollTop = msglog.scrollHeight;
});

export const input = html('textarea', { id: 'chat_input', placeholder: 'Press enter to chat' });
input.addEvent('keydown', e => {
  if(e.key == 'Enter' && !e.shiftKey) {
    if(!e.shiftKey) e.preventDefault();
    if(!input.value.trim() || Date.now() - lmd < 500) return;
    lmd = Date.now();
    send(input.value);
    input.value = '';
  }
});
input.setAttribute('aria-label', 'Chat input');
win.container.appendChild(input);

export function append(title, message) {
  const div = html('div', {
    className: 'chat_message', title: new Date()
  });
  if(message) div.appendChild(html('b', { innerHTML: title + ':\u00a0' }));
  div.insertAdjacentHTML('beforeend', message || title);
  msglog.appendChild(div);
  msglog.scrollTop = msglog.scrollHeight;
}

export function send(msg) {
  if(msg.startsWith('/nick')) {
    const nn = msg.slice(6, 6 + 30);
    if(localStorage.nickname !== nn) localStorage.nickname = nn;
  }
  wsSend(PacketWriter.s_chat_message(msg.slice(0, 1000)));
}

on('open', () => localStorage.nickname && send('/nick ' + localStorage.nickname));