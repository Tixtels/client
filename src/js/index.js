/* eslint no-console: "off" */
/* global __VERSION__ */

console.log('%cTixtels %cv' + __VERSION__, 'color: darkblue; font-size: xx-large; font-weight: bold;', '');
console.log('Copyright \u00a9 2019');

import * as windows from './windows.js';
import * as camera from './camera.js';
import * as utils from './utils.js';
import * as ws from './ws.js';
import { options } from './data.js';
import tixtel from './tixtel.js';
import * as tools from './tools.js';
import * as data from './data.js';
import * as chunks from './chunks.js';
import chunk from './chunk.js';
import * as net from './net.js';
import * as chat from './chat.js';
import * as mouse from './mouse.js';
import * as keyboard from './keyboard.js';
import * as colors from './colors.js';
import ranks from '../../common/ranks.json';
import tooloption from './tooloption.js';

camera.centerTo(0, 0);

const modules = {
  camera, chat, chunk, chunks, colors, data, keyboard, mouse, net, options, ranks, tixtel, tooloption, tools, utils, windows, ws
};

window.require = module => {
  if(module === '*') return modules;
  if(!modules.hasOwnProperty(module)) throw new Error('Unknown module provided.');
  return modules[module];
};

require('./deftools.js');
require('./menu.js');