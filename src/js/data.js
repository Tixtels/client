import { chunkSize, defBlockSize as blockSize, spawnX as x, spawnY as y, defPalette } from '../../config.json';
export { chunkSize, defPalette };

export const offsets = { x, y }, chunks = {},
  game = document.getElementById('game'), ctx = game.getContext('2d'),
  options = {
    showGrid: true,
    cameraSpeed: 32,
    blockSize,
    allowTextPaste: false,
    rank: -1
  };