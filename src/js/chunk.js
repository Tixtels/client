import { chunkSize, offsets, options, game } from './data.js';
import Tixtel from './tixtel.js';

export default class Chunk {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.blocks = [];
    this.genBlocks();
    this.protected = false;
    this.canvas = document.createElement('canvas');
    this.resizeCanvas();
    this.ctx = this.canvas.getContext('2d');
    this.ctx.textBaseline = 'middle';
    this.ctx.textAlign = 'center';
  }

  resizeCanvas() {
    this.canvas.width = this.canvas.height = options.blockSize * 16;
    this.blocks.forEach(b => {
      b.renderX = b.x * options.blockSize;
      b.renderY = b.y * options.blockSize;
    });
  }

  updateCanvas() {
    this.resizeCanvas();
    this.reRender();
  }

  genBlocks() {
    for(let x = 0; x < chunkSize; x++) {
      for(let y = 0; y < chunkSize; y++) {
        this.blocks.push(new Tixtel(x, y, this));
      }
    }
  }

  isVisible() {
    return ((offsets.x + (this.x - 1) * chunkSize * options.blockSize) <  game.width ) &&
           ((offsets.x + (this.x + 1) * chunkSize * options.blockSize) >=     0      ) &&
           ((offsets.y + (this.y - 1) * chunkSize * options.blockSize) <  game.height) &&
           ((offsets.y + (this.y + 1) * chunkSize * options.blockSize) >=     0      );
  }

  reRender() {
    this.blocks.forEach(b => b.render());
  }

  setData(data) {
    data.forEach((xcol, x) => {
      xcol.forEach((info, y) => {
        const b = this.blocks[y * chunkSize + x];
        b.color = info.rgb;
        b.char = String.fromCodePoint(info.char);
        b.render();
      });
    });
  }
}