import * as Windows from './windows.js';
import { html } from './utils.js';

import { toolKeybinds } from './keyboard.js';

const win = Windows.add(class extends Windows.window {
  constructor() {
    super('Tools', {
      closeable: false,
      movable: true
    });
  }
});

export const all = [];
export let current = null;

export function add(tool, div = win.container) {
  if(!(tool.prototype instanceof toolClass)) throw new Error('Invalid tool provided');
  tool = new tool;
  all.push(tool);
  tool.d = html('div', {
    className: 'tool' + (tool.opts.default ? ' active' : ''),
    title: tool.name // TODO custom title element
  });
  tool.d.appendChild(html('div', {
    className: 'icon',
    style: 'background-image: url("' + tool.opts.icon + '");'
  }));
  tool.d.addEvent('click', () => select(tool));
  div.appendChild(tool.d);
  if(tool.opts.default) {
    current = tool;
    select(tool);
  }
  return tool;
}

export function remove(tool) {
  if(!(tool instanceof toolClass)) throw new Error('Invalid tool provided');
  const index = all.findIndex(findTool => findTool === tool);
  if(index === -1) throw new Error('Unknown tool provided');
  tool.d.remove();
  all.splice(index, 1);
  if(current === tool) {
    if(index === 0) select(all[0]);
    else select(all[index - 1]);
  }
}

export function select(tool) {
  if(!(tool instanceof toolClass)) throw new Error('Invalid tool provided');
  if(tool === current) return;
  current.d.classList.remove('active');
  call('unselected');
  current = tool;
  call('selected');
  tool.d.classList.add('active');
}

export function call(event, ...data) {
  if(current[event]) current[event](...data);
}

export function apply(event, args) {
  if(current[event]) current[event].apply(current[event], args);
}

export const tool = class Tool {
  constructor(name, opts = {default: false}) {
    this.name = name;
    this.opts = opts;
    if(!this.opts.icon) this.opts.icon = 'https://cdn.glitch.com/dd47f749-4f4e-455e-8b70-b81881c6492b%2Funknown.png?1545575314280';
    if(this.opts.keybind) toolKeybinds[this.opts.keybind] = this;
  }
};

const toolClass = tool;