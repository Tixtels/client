function getParam(name) {
  name = name.replace(/[[]]/g, '\\$&');
  const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(location.href);
  if(!results) return null;
  if(!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

export let address = getParam('ip') || 'wss://tixtels.glitch.me';
const events = {};

export let ws;

let onClose = () => { if(confirm('Disconnected from server, press OK to reconnect.')) connect(); };

export function connect(wsAddress) {
  if(wsAddress) address = wsAddress;
  ws = new WebSocket(address);
  ws.binaryType = 'arraybuffer';
  ws.addEventListener('close', onClose);
  for(let e in events) events[e].forEach(v => ws.addEventListener(e, v));
} connect();

export function send(data) {
  if(ws.readyState === 1) return ws.send(data);
  // may hit rate-limits
  else ws.addEventListener('open', () => send(data), { once: true });
}

export function reconnect(wsAddress = address) {
  if(ws.readyState !== 1) throw new Error('Not connected, please use WS.connect instead.');
  const oc = onClose;
  onClose = () => {
    address = wsAddress;
    connect();
    onClose = oc;
  };
  ws.close();
}

export function disconnect() { ws.close(); }

export function on(e, fn) {
  if(!events.hasOwnProperty(e)) events[e] = [];
  events[e].push(fn);
  ws.addEventListener(e, fn);
}